/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.Serializable;

/**
 *
 * @author informatics
 */
public class Friend implements Serializable {
    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastId=1;
    
    public Friend(String name,String tel,int age){
        this.id = lastId;
        this.tel = tel;
        this.age = age;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {
        if(age<0){
            throw new Exception();
        }
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public static int getLastId() {
        return lastId;
    }

    public static void setLastId(int lastId) {
        Friend.lastId = lastId;
    }

    
    
    @Override
    public String toString() {
        return "Friend{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }
    
    
    
    public static void main(String[] args) throws Exception {
        try {
            Friend f1 =new Friend("Pom","123456789",18);
         Friend f2 =new Friend("ICE","123456789",18);
         System.out.println(f1.toString());
         System.out.println(f2.toString());
         f1.setAge(10);
        } catch(Exception ex){
            System.out.println("Shit");
        }
         
         
    }
}
